//
//  ViewController.m
//  01
//
//  Created by Zach Eriksen on 8/24/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *data;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _data = @[@"one",
              @"two",
              @"three"];
    
    [_tableView setDelegate: self];
    [_tableView setDataSource: self];
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"basicCell"];
    
    NSString *text = [_data objectAtIndex: [indexPath row]];
    [[cell textLabel] setText:text];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_data count];
}

@end
