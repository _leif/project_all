//
//  ViewController.m
//  01
//
//  Created by Zach Eriksen on 8/22/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *innerView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_innerView setBackgroundColor: [UIColor greenColor]];
    [_innerView setTransform: CGAffineTransformMakeRotation(180)];
    
    [[_innerView layer] setCornerRadius: 16];
    [[_innerView layer] setShadowOpacity: 1];
    [[_innerView layer] setShadowColor:[UIColor darkGrayColor].CGColor];
    
}


@end
