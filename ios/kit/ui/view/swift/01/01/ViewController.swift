//
//  ViewController.swift
//  01
//
//  Created by Zach Eriksen on 8/22/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var innerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        innerView.backgroundColor = .green
        innerView.layer.cornerRadius = 16
        
        innerView.layer.shadowOpacity = 1
        innerView.layer.shadowColor = UIColor.darkGray.cgColor
        
        innerView.transform = innerView.transform.rotated(by: 180)
    }


}

