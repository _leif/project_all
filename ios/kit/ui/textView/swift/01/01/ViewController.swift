//
//  ViewController.swift
//  01
//
//  Created by Zach Eriksen on 8/28/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var height: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        textView.delegate = self
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
    @objc
    private func dismissKeyboard() {
        textView.resignFirstResponder()
    }
    
    fileprivate func animateConstraintChange(withDuration duration: Double) {
        UIView.animate(withDuration: duration) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        height.constant *= 10
        animateConstraintChange(withDuration: 1.5)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        height.constant = 60
        animateConstraintChange(withDuration: 0.5)
    }
}

