//
//  ViewController.m
//  01
//
//  Created by Zach Eriksen on 8/28/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;

@end

@implementation ViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_textView setDelegate: self];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(dismissKeyboard)];
    [[self view] addGestureRecognizer: tap];
}

- (void)dismissKeyboard {
    [_textView resignFirstResponder];
}

- (void)animateConstraintChange:(double)duration {
    [UIView animateWithDuration: duration animations: ^{
        [[self view] layoutIfNeeded];
    }];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [_height setConstant: _height.constant * 10];
    [self animateConstraintChange: 1.5];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [_height setConstant: 60];
    [self animateConstraintChange: 0.5];
}


@end
