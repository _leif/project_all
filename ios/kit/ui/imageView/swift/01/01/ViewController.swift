//
//  ViewController.swift
//  01
//
//  Created by Zach Eriksen on 8/22/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = UIImage(named: "image")!
        imageView.contentMode = .scaleToFill
        
        imageView.layer.cornerRadius = 16
        imageView.layer.borderWidth = 3
        imageView.layer.borderColor = UIColor.green.cgColor
        
        imageView.clipsToBounds = true
    }
}

