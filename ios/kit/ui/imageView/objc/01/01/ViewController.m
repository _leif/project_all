//
//  ViewController.m
//  01
//
//  Created by Zach Eriksen on 8/22/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_imageView setImage: [UIImage imageNamed:@"image"]];
    [_imageView setContentMode: UIViewContentModeScaleToFill];
    
    [[_imageView layer] setCornerRadius: 16];
    [[_imageView layer] setBorderWidth: 3];
    [[_imageView layer] setBorderColor: [UIColor greenColor].CGColor];
    
    [_imageView setClipsToBounds: YES];
}


@end
