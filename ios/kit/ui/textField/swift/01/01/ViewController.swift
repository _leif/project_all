//
//  ViewController.swift
//  01
//
//  Created by Zach Eriksen on 8/24/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textField.autocapitalizationType = .allCharacters
        textField.borderStyle = .roundedRect
        textField.backgroundColor = .yellow
    }
}

