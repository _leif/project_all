//
//  ViewController.m
//  01
//
//  Created by Zach Eriksen on 8/26/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_textField setAutocapitalizationType: UITextAutocapitalizationTypeAllCharacters];
    [_textField setBorderStyle: UITextBorderStyleRoundedRect];
    [_textField setBackgroundColor: [UIColor yellowColor]];
}


@end
