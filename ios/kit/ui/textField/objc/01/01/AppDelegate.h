//
//  AppDelegate.h
//  01
//
//  Created by Zach Eriksen on 8/26/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

