//
//  ViewController.swift
//  button_01
//
//  Created by Zach Eriksen on 8/22/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button.setTitle("Hello World", for: .normal)
        button.setTitleColor(.orange, for: .normal)
        button.setTitleShadowColor(.darkGray, for: .normal)
        button.addTarget(self, action: #selector(tappedButton), for: .touchUpInside)
    }
    
    @objc
    func tappedButton() {
        let newAlpha = CGFloat.random(in: 0 ... 1)
        UIView.animate(withDuration: 0.25) {
            self.button.alpha = newAlpha
        }
        print(newAlpha)
    }
}

