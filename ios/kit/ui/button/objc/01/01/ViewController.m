//
//  ViewController.m
//  01
//
//  Created by Zach Eriksen on 8/22/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_button setTitle:@"Hello World" forState:UIControlStateNormal];
    [_button setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [_button setTitleShadowColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(buttonTapped) forControlEvents:UIControlEventTouchUpInside];
}

- (void)buttonTapped {
    srand48(time(0));
    CGFloat newAlpha = drand48();
    [UIView animateWithDuration:0.25 animations:^{
        [self->_button setAlpha:newAlpha];
    }];
    printf("%f\n", newAlpha);
}


@end
