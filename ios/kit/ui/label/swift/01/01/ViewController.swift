//
//  ViewController.swift
//  01
//
//  Created by Zach Eriksen on 8/22/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label.backgroundColor = .orange
        label.textColor = .white
        label.text = "Label.text"
    }
}

