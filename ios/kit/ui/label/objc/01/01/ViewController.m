//
//  ViewController.m
//  01
//
//  Created by Zach Eriksen on 8/22/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_label setTextColor:[UIColor whiteColor]];
    [_label setText:@"Label.text"];
    [_label setBackgroundColor:[UIColor orangeColor]];
}


@end
