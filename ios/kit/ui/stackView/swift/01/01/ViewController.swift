//
//  ViewController.swift
//  01
//
//  Created by Zach Eriksen on 8/28/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func oneTapped(_ sender: UIButton) {
        sender.isHidden = !sender.isHidden
    }
}
